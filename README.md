# OpenStack Control Plane at CERN

This is an umbrella chart managing the OpenStack control plane deployments at
CERN.

## Pre-Requisites

First get a Kubernetes cluster up and running, [check here](https://clouddocs.web.cern.ch/containers/quickstart.html).

Make sure you also [deploy Helm](https://clouddocs.web.cern.ch/containers/tutorials/helm.html).

## Deployment

```bash
helm repo list
fluxcd          https://charts.fluxcd.io  
...

helm install fluxcd/helm-operator --namespace flux --name helm-operator   --version 0.4.0 --values helm-operator-values.yaml 
helm install fluxcd/flux --namespace flux --name flux --values flux-values.yaml --set git.url=https://gitlab.cern.ch/helm/releases/openstack --set git.branch=strigazi-heattn
```
